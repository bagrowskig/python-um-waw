from __future__ import unicode_literals, absolute_import, print_function

import ldap
from django_auth_ldap.config import LDAPSearch, GroupOfUniqueNamesType


# AUTHENTICATION_BACKENDS = (
#     'django_auth_ldap.backend.LDAPBackend',
#     'django.contrib.auth.backends.ModelBackend',
# )

AUTH_LDAP_SERVER_URI = "ldap://ldap.forumsys.com"

# AUTH_LDAP_CONNECTION_OPTIONS = {
#     ldap.OPT_REFERRALS: 0,
# }

AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,dc=example,dc=com"

AUTH_LDAP_USER_ATTR_MAP = {
    "last_name": "sn",
    "email": "mail",
}

AUTH_LDAP_USER_SEARCH = LDAPSearch(
    "ou=users,dc=example,dc=com",
    ldap.SCOPE_SUBTREE,
    "(uid=%(user)s)"
)


AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_active": [
        "ou=scientists,dc=example,dc=com",
        "ou=mathematicians,dc=example,dc=com",
    ],
    "is_staff": [
        "ou=mathematicians,dc=example,dc=com",
        "ou=scientists,dc=example,dc=com",
    ],
    "is_superuser": [
        "ou=mathematicians,dc=example,dc=com",
        "ou=scientists,dc=example,dc=com",
    ]
}


AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    "ou=scientists,dc=example,dc=com",
    ldap.SCOPE_SUBTREE,
    "(objectClass=groupOfUniqueNames)"
)
AUTH_LDAP_GROUP_TYPE = GroupOfUniqueNamesType()

AUTH_LDAP_MIRROR_GROUPS = True

import logging

logger = logging.getLogger('django_auth_ldap')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)
