from __future__ import unicode_literals, absolute_import, print_function

import os

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)


PIPELINE = {
    'PIPELINE_ENABLED': False,
    'JAVASCRIPT': {
        'base': {
            'source_filenames': (
                'lib/jquery/jquery-1.11.2.js',
                'lib/bootstrap-3.3.6-dist/js/bootstrap.js',
            ),
            'output_filename': 'base.min.js',
        },
    },
    'STYLESHEETS': {
        'base': {
            'source_filenames': (
                'lib/bootstrap-3.3.6-dist/css/bootstrap.css',
                'css/common/base.css',
            ),
            'output_filename': 'base.min.css',
        },
    }
}
PIPELINE['CSS_COMPRESSOR'] = 'pipeline.compressors.NoopCompressor'
PIPELINE['JS_COMPRESSOR'] = 'pipeline.compressors.NoopCompressor'
