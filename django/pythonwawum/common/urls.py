from __future__ import unicode_literals, absolute_import, print_function

from django.conf.urls import url

from .views import HomeView


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
]
