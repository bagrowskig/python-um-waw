from __future__ import unicode_literals, absolute_import, print_function

from django.views.generic import TemplateView


class HomeView(TemplateView):

    template_name = 'common/home.html'
