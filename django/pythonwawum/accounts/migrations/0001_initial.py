# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import model_utils.fields
import django_extensions.db.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('uuid', django_extensions.db.fields.ShortUUIDField(verbose_name='uuid', editable=False, blank=True)),
                ('name', models.CharField(max_length=256, verbose_name='name')),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'position',
                'verbose_name_plural': 'positions',
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('uuid', django_extensions.db.fields.ShortUUIDField(verbose_name='uuid', editable=False, blank=True)),
                ('gender', models.CharField(max_length=1, verbose_name='gender', choices=[('m', 'Male'), ('f', 'Female')])),
                ('position', models.ForeignKey(verbose_name='position', to='accounts.Position')),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'profile',
                'verbose_name_plural': 'profiles',
            },
        ),
    ]
