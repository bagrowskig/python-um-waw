from __future__ import absolute_import, unicode_literals, print_function

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AccountsAppConfig(AppConfig):
    name = 'accounts'
    verbose_name = _('Accounts')

    def ready(self):
        from . import signals
