from __future__ import unicode_literals, absolute_import, print_function

from django.utils.translation import ugettext_lazy as _


class GenderEnum(object):

    MALE = 'm'
    FEMALE = 'f'

    values = (
        MALE,
        FEMALE,
    )

    display_names = {
        MALE: _('Male'),
        FEMALE: _('Female'),
    }

    choices = (
        (MALE, display_names[MALE]),
        (FEMALE, display_names[FEMALE])
    )
