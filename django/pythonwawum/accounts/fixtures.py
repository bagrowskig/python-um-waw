# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals, print_function


from .models import Position


POSITIONS = (
    {'name': 'Prezes'},
    {'name': 'Księgowa'},
    {'name': 'Programista'},
    {'name': 'Informatyk'},
    {'name': 'Stażysta'},
)


def create_positions(data=POSITIONS):
    for position in data:
        Position.objects.create(**position)
