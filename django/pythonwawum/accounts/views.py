from __future__ import unicode_literals, absolute_import, print_function

from django.views.generic import (
    CreateView, UpdateView, DetailView, ListView,
)
from .forms import ProfileForm
from .models import Profile, Position


class ProfileViewMixin(object):
    model = Profile
    form_class = ProfileForm


class ProfileCreateView(ProfileViewMixin, CreateView):
    pass


class ProfileUpdateView(ProfileViewMixin, UpdateView):
    pass


class ProfileDetailView(ProfileViewMixin, DetailView):
    pass


class ProfileListView(ProfileViewMixin, ListView):
    paginate_by = 5

    def get_queryset(self):
        return (
            super(ProfileListView, self)
            .get_queryset()
            .only('position', 'gender')
            .select_related('position')
        )


class PositionListView(ListView):
    model = Position

    def get_queryset(self):
        return (
            super(PositionListView, self)
            .get_queryset()
            # .only('position', 'gender')
            .prefetch_related('profile_set')
        )
