from __future__ import unicode_literals, absolute_import, print_function

from django_auth_ldap.backend import populate_user
from django.dispatch import receiver


@receiver(populate_user)
def populate_user_from_ldap(
        sender, signal, user=None, ldap_user=None, **kwargs):
    try:
        user.first_name = ldap_user.attrs['cn'][0].split()[0]
    except (KeyError, IndexError):
        pass
