from __future__ import unicode_literals, absolute_import, print_function

from django.conf.urls import url

from .views import (
    ProfileCreateView, ProfileDetailView, ProfileListView, ProfileUpdateView,
    PositionListView,
)

urlpatterns = [
    url(r'^list/$', ProfileListView.as_view(), name='profile__list'),
    url(r'^position/list/$', PositionListView.as_view(), name='position__list'),
    url(r'^detail/(?P<pk>(\d+))/$', ProfileDetailView.as_view(), name='profile__detail'),
    url(r'^create/$', ProfileCreateView.as_view(), name='profile__create'),
    url(r'^edit/(?P<pk>(\d+))/$', ProfileUpdateView.as_view(), name='profile__edit'),
]
