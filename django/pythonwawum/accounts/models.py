from __future__ import unicode_literals, absolute_import, print_function

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django_extensions.db.fields import ShortUUIDField

from .enums import GenderEnum


class BaseModel(TimeStampedModel, models.Model):

    uuid = ShortUUIDField(_('uuid'), editable=False)

    class Meta:
        abstract = True


class Position(BaseModel):

    name = models.CharField(_('name'), max_length=256)

    class Meta:
        verbose_name = _('position')
        verbose_name_plural = _('positions')
        ordering = (
            'created',
        )

    def __unicode__(self):
        return "{name}".format(name=self.name)


class Profile(BaseModel):

    position = models.ForeignKey(Position, verbose_name=_('position'))
    gender = models.CharField(
        _('gender'), choices=GenderEnum.choices, max_length=1)

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')
        ordering = (
            'created',
        )

    def __unicode__(self):
        return "{position} {gender}".format(
            position=self.position.name, gender=self.gender,
        )

    def get_absolute_url(self):
        return reverse('profile__detail', kwargs=dict(pk=self.pk))
