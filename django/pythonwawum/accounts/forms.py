from __future__ import unicode_literals, absolute_import, print_function

from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms import layout, bootstrap
from crispy_forms.helper import FormHelper

from .enums import GenderEnum
from .models import Profile


class ProfileForm(forms.ModelForm):

    secret_password = forms.CharField(label=_('secret password'))

    class Meta:
        model = Profile
        fields = (
            'position',
            'gender',
            'secret_password',
        )

    def clean_secret_password(self):
        value = self.cleaned_data.get('secret_password')
        if value == 'show_errors':
            self.add_error('position', 'An error!')
            self.add_error('gender', 'Tadaaa')
            self.add_error('__all__', 'Tadaaa')
            raise forms.ValidationError('Invalid secret')
        return value

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['position'].queryset = (
            self.fields['position'].queryset
            .exclude(name__startswith='K')
        )

    @property
    def helper(self):
        helper = FormHelper()
        helper.layout = layout.Layout(
            layout.Fieldset(
                _('Profile information'),
                'position',
                'gender',
                'secret_password',
            ),
            bootstrap.FormActions(
                layout.Submit('save', _('Save'), css_class="btn-primary"),
            ),
        )
        return helper
