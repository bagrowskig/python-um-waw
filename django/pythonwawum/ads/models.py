from __future__ import unicode_literals, absolute_import, print_function

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django_extensions.db.fields import ShortUUIDField


class Ad(TimeStampedModel, models.Model):

    uuid = ShortUUIDField(_('uuid'), editable=False)
    subject = models.CharField(_('subject'), max_length=256)
    description = models.CharField(
        _('description'), max_length=1024,
        default='', blank=True,
    )
    email = models.EmailField(_('email'), default='', blank=True)
    picture = models.ImageField(_('picture'), null=True, blank=True)

    class Meta:
        verbose_name = _('ad')
        verbose_name_plural = _('ads')
        ordering = (
            'created',
        )

    def __unicode__(self):
        return "{created} {uuid} {subject} {email}".format(
            created=self.created,
            uuid=self.uuid[:8],
            subject=self.subject[:15],
            email=self.email,
        )
