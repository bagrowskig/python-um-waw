from __future__ import unicode_literals, absolute_import, print_function

from django.conf.urls import url

from .views import current_datetime, ads_list, AdsList, AdCreateView


urlpatterns = [
    url(r'^date/', current_datetime, name='current_datetime'),
    url(r'^list/f/', ads_list, name='ads_list'),
    url(r'^list/', AdsList.as_view(), name='ads_list'),
    url(r'^create/', AdCreateView.as_view(), name='ads_create'),
]
