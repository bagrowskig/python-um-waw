from __future__ import unicode_literals, absolute_import, print_function

from django import forms
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms import layout, bootstrap

from .models import Ad


class AdForm(forms.ModelForm):

    class Meta:
        model = Ad
        fields = (
            'subject',
            'description',
            'picture',
            'email',
        )

    def fun_helper(self):
        pass

    @property
    def helper(self, *args, **kwargs):
        helper = FormHelper()

        # TODO: form-horizontal is not automatically added to the form class
        helper.form_class = 'form-horizontal print-friendly-form'
        helper.label_class = self.label_class = 'col-sm-3 col-xs-4'
        helper.field_class = self.field_class = 'col-sm-7 col-xs-6'
        sections = (
            layout.Fieldset(
                _('Contact information'),
                'email',
            ),
            layout.Fieldset(
                _('Ad'),
                'subject',
                'description',
            ),
            layout.Fieldset(
                _('Attachments'),
                'picture',
            ),
            bootstrap.FormActions(
                layout.Submit('create', _('Create'), css_class="btn-primary"),
            ),
        )
        helper.layout = layout.Layout(*sections)
        return helper
