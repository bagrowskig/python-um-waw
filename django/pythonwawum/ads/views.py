from __future__ import unicode_literals, absolute_import, print_function

import datetime
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import ListView, CreateView
from django.core.urlresolvers import reverse_lazy

# from ads.models import Ad
from .forms import AdForm
from .models import Ad


class AdCreateView(CreateView):

    form_class = AdForm
    model = Ad
    success_url = reverse_lazy('ads_list')


class AdsList(ListView):

    model = Ad
    # 'ads/ad_list.html'
    # '<app_name>/<model_name>_list.html'


def ads_list(request):
    return render_to_response(
        'ads/ads_list.html',
        {
            'objects': Ad.objects.all(),
        }
    )


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)
