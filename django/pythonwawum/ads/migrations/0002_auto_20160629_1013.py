# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='description',
            field=models.CharField(default='', max_length=1024, verbose_name='description', blank=True),
        ),
        migrations.AlterField(
            model_name='ad',
            name='email',
            field=models.EmailField(default='', max_length=254, verbose_name='email', blank=True),
        ),
        migrations.AlterField(
            model_name='ad',
            name='picture',
            field=models.ImageField(upload_to=b'', null=True, verbose_name='picture', blank=True),
        ),
    ]
