# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('uuid', django_extensions.db.fields.ShortUUIDField(verbose_name='uuid', editable=False, blank=True)),
                ('subject', models.CharField(max_length=256, verbose_name='subject')),
                ('description', models.CharField(max_length=1024, verbose_name='description')),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
                ('picture', models.ImageField(upload_to=b'', verbose_name='picture')),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'ad',
                'verbose_name_plural': 'ads',
            },
        ),
    ]
