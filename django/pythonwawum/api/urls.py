from __future__ import absolute_import, unicode_literals, print_function

from django.conf.urls import patterns, url, include
from rest_framework import routers

from .views import AdViewSet

router = routers.DefaultRouter()
router.register(r'ad', AdViewSet)


urlpatterns = patterns(
    '',
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^ads/', include(router.urls)),
)
