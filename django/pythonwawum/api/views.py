from __future__ import absolute_import, unicode_literals, print_function

# import django_filters

from rest_framework import viewsets, serializers, filters


from ads.models import Ad


class AdFilter(filters.FilterSet):

    class Meta:
        model = Ad
        fields = (
            'subject',
            'id',
        )


class AdSerializer(serializers.ModelSerializer):

    date = serializers.SerializerMethodField()

    class Meta:
        model = Ad
        # fields = (
        #     'created',
        #     'date',
        # )

    def get_date(self, obj):
        # a completely custom method field
        from django.utils.timezone import now
        return now()


class AdViewSet(viewsets.ModelViewSet):
    # use `uuid` instead of `id`
    lookup_field = 'uuid'
    queryset = Ad.objects.all()
    serializer_class = AdSerializer
    filter_class = AdFilter

    filter_backends = (
        filters.SearchFilter,
        filters.DjangoFilterBackend,
    )
    # use ?search=query
    search_fields = ('subject', 'email')
