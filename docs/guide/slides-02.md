# django tutorial #2

## git

we will use:

* git clone
* git branch
* git commit
* git push

The tutorial repository:

bitbucket: https://bitbucket.org/bagrowskig/python-um-waw

clone it using git:

```bash
git clone https://bagrowskig@bitbucket.org/bagrowskig/python-um-waw.git
```

## contributing using git

create your own branch:

```bash
git branch <initials>-<feature>
git branch gb-db_tutorial
```

make a commit
```bash
# make some change
git add -i
git commit
```

push it to the remote repository
```bash
git push
```

go to bitbucket to see your branch:

https://bitbucket.org/bagrowskig/python-um-waw/branches/


## virtualenv

virtualenv/virtualenvwrapper

* mkvirtualenv env-name
* workon
* deactivate
* cd $WORKON_HOME
* "$WORKON_HOME/env-name/bin/python"
* "$WORKON_HOME/env-name/lib/python2.7/site-packages"

pip

* pip
* pip install
* pip freeze
* pip freeze | grep Django

install requirements

```bash
pip install -r deployment/python/base.txt
pip install -r deployment/python/development.txt
```

## Django

basic concepts

models

* model
* manager
* settings.DATABASES

urls

* ROOT_URLS
* <appname>/urls.py
* include('appname.urls')

views

* Class based views
* Function based views

templates
* django template syntax


## static files

django-pipeline

```bash
pip install django-pipeline
```

```python
INSTALLED_APPS = (
    ...
    'pipeline',
    ...
)
```

```python
from __future__ import absolute_import, unicode_literals, print_function

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.NoopCompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.NoopCompressor'

PIPELINE_JS = {}
PIPELINE_CSS = {}

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
)
```

## static files

```python
PIPELINE_JS = {

    'base': {
        'source_filenames': (
            'lib/jquery-2.1.4.js',
            'lib/bootstrap-3.3.5-dist/js/bootstrap.js',
            'lib/lodash-3.10.0.js',
        ),
        'output_filename': 'base.min.js',
    },
}

PIPELINE_CSS = {
    'base': {
        'source_filenames': (
            'lib/bootstrap-3.3.5-dist/css/bootstrap.css',
            'css/common/base.css',
        ),
        'output_filename': 'base.min.css',
    },
}
```


## templates

* {{ variable }}
* {{ variable|filter:"filter_arg" }}
* {{% tag %}}
* {{% for item in collection %}{{ item }}{% endfor %}}
* {% block main %}{% endblock %}



## databases

Official backends

Postgresql 9.0+

https://docs.djangoproject.com/en/1.8/ref/databases/#postgresql-notes

MySQL 5.5+

https://docs.djangoproject.com/en/1.8/ref/databases/#mysql-notes

Oracle 11.1+

https://docs.djangoproject.com/en/1.8/ref/databases/#oracle-notes

MSSQL - 3rd party backend

http://django-mssql.readthedocs.io/en/latest/

http://django-mssql.readthedocs.io/en/latest/changelog.html


## inspectdb

empty file
```python
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
```

## inspectdb

Ad model according do inspectdb (ads/models.py)

```python
[...]
from __future__ import unicode_literals

from django.db import models

class AdsAd(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    created = models.DateTimeField()
    modified = models.DateTimeField()
    uuid = models.CharField(max_length=22)
    subject = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)
    email = models.CharField(max_length=254)
    picture = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ads_ad'
```

## inspectdb

Actual model
```python
from __future__ import unicode_literals, absolute_import, print_function

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django_extensions.db.fields import ShortUUIDField


class Ad(TimeStampedModel, models.Model):

    uuid = ShortUUIDField(_('uuid'), editable=False)
    subject = models.CharField(_('subject'), max_length=256)
    description = models.CharField(
        _('description'), max_length=1024,
        default='', blank=True,
    )
    email = models.EmailField(_('email'), default='', blank=True)
    picture = models.ImageField(_('picture'), null=True, blank=True)

    class Meta:
        verbose_name = _('ad')
        verbose_name_plural = _('ads')
        ordering = (
            'created',
        )
```


## LDAP

https://bitbucket.org/psagers/django-auth-ldap/wiki/Home


## Using separate database for auth

First of all a database router would be required:
https://docs.djangoproject.com/en/1.9/topics/db/multi-db/#an-example

But it's not that simple:

```
"This example is intended as a demonstration of how the router
infrastructure can be used to alter database usage. It intentionally
ignores some complex issues in order to demonstrate how routers are used."
```

From: https://docs.djangoproject.com/en/1.9/topics/db/multi-db/#behavior-of-contrib-apps

Several contrib apps include models, and some apps depend on others. Since cross-database relationships are impossible, this creates some restrictions on how you can split these models across databases:

* each one of contenttypes.ContentType, sessions.Session and sites.Site can be stored in any database, given a suitable router.
* auth models — User, Group and Permission — are linked together and linked to ContentType, so they must be stored in the same database as ContentType.
* admin depends on auth, so their models must be in the same database as auth.

So, anything using ContentTypes will need to stay in that database as well.

Another drawback is that we'd pollute the target database with some django models.

## Postgresql foreign-data wrapper

If the database containing users happens to be postgresql as well, we could use postgresql-fdw extension:

https://www.postgresql.org/docs/9.3/static/postgres-fdw.html

It creates a transaction-safe mirror of certain tables stored in another database.

This solution could be transparent to django. We'd only need to customize the User model to adapt to a custom table and perhaps add different password handling.


## Django user model

Most likely, custom user table will differ from what django is expecting. This problem is described well in legacy database integration: https://docs.djangoproject.com/en/1.9/howto/legacy-databases/.

```python
class MyModel(models.Model):

    first_name = models.CharField(db_column='name', max_length=30)
    last_name = models.CharField(db_column='surname, max_length=30)
    email = models.EmailField(db_column='email_address')

    class Meta:
        # disable migrations
        managed = False
        # custom table name
        db_table = 'users'
```

## Django user model #2

After adapting our model to the legacy user database now we need to integrate it with django auth.

It's likely that it would be enough to inherit from `AbstractUser`, as described here:

http://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html

Then, modify the default password hashers:
https://docs.djangoproject.com/en/1.8/ref/settings/#std:setting-PASSWORD_HASHERS

Or even create your own to match the legacy setup:
https://docs.djangoproject.com/en/1.8/topics/auth/passwords/#writing-your-own-hasher

## SSO

Another approach would be to setup a central Single sign-on server.

Example implementation for django: https://github.com/aldryn/django-simple-sso

Login process as described (https://github.com/aldryn/django-simple-sso#workflow):
* User wants to log into a Client by clicking a "Login" button. The initially requested URL can be passed using the next GET parameter.
* The Client's Python code does a HTTP request to the Server to request a authentication token, this is called the Request Token Request.
* The Server returns a Request Token.
* The Client redirects the User to a view on the Server using the Request Token, this is the Authorization Request.
* If the user is not logged in the the Server, they are prompted to log in.
* The user is redirected to the Client including the Request Token and a Auth Token, this is the Authentication Request.
* The Client's Python code does a HTTP request to the Server to verify the Auth Token, this is called the Auth Token Verification Request.
* If the Auth Token is valid, the Server returns a serialized Django User object.
* The Client logs the user in using the Django User recieved from the Server.

# Thank you for listening
