# Linux


## System & terminal

https://help.ubuntu.com/community/KeyboardShortcuts

Terminal
* ctrl+alt+t
* ctrl+c
* ctrl+l, clear
* ctrl+shift+c, ctrl+shift+v
* ctrl+r, ctrl+s
* man, help
* cd ~/, cd -, cd ..
* mkdir, cp, mv
* ls, ll, du, df
* cat, more, less, tail, grep, touch
* apt, pip, npm, gem

## Bash, python

* perms: u+x
* pipe, | > >>
* python test.py
* ./test.py
* `#!/usr/bin/env bash`
* `#!/usr/bin/env python`
* stderr, stdout

## Example: bash/python script

## Narzędzia

* top, htop, pg_top
* screen, tmux
* vim
* tail -f, lnav
* curl, wget, scp

## Usługi

* ps aux | grep django
* kill, pkill -f
* service status|start|stop|reload
* tail -f /var/log/*
* tail -f /var/log/syslog

# Git

## Git tools

* jak działa git
* git diff, git diff --cached
* git push, git pull --rebase
* git log
* git blame
* git stash [pop|list|apply]
* git add -i
* git rebase -i HEAD^^^^
* git commit --amend
* git push --force (!)

## Git review flow

1. branch from master
2. make changes
3. rebase updated master to branch
4. push to branch
5. pull request
6. review
7. merge
8. pull master

## Example: git flow

# Python environment

## virtualenv

* virtualenv
* virtualenvwrapper

http://virtualenvwrapper.readthedocs.io/en/latest/install.html

* workon
* deactivate
* cd $WORKON_HOME
* "$WORKON_HOME/env-name/bin/python"
* "$WORKON_HOME/env-name/lib/python2.7/site-packages"
* "$WORKON_HOME/env-name/src"

## pip
* pip
* pip install
* pip freeze
* pip freeze > env.txt
* setup.py

Versioning:

    pytz
    django-extensions==1.5.1
    Django>=1.8.2,<1.9.0
    -e git+https://bagrowskig@bitbucket.org/bagrowskig/py-rich-enum.git@a62294b549f9b8478b486359469a3d63558ef744#egg=py_rich_enum-master


# Python

## import this

The Zen of Python, by Tim Peters

    Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one-- and preferably only one --obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than *right* now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea -- let's do more of those!


## pep8, pep257

Code style guide

https://www.python.org/dev/peps/pep-0008/

Docstring style guide

https://www.python.org/dev/peps/pep-0257/

Python guide

http://docs.python-guide.org/en/latest/writing/style/

Google style guide

https://google.github.io/styleguide/pyguide.html

Linters

* pychecker - executes (be careful)
* pyflakes - parses, great for finding NameErrors, obsolete imports
* pylint - parses, very comprehensive (on the excessive-compulsive side)
* pep8 - parses, a style checker
* flake8 - parser, combines pep8 and pyflakes, with added complexity support, extensible

## pep8 (https://www.python.org/dev/peps/pep-0008/)

## pep257 (https://www.python.org/dev/peps/pep-0257/)

## Example: linter

## .editorconfig

    root = true

    [*]
    end_of_line = lf
    insert_final_newline = true
    trim_trailing_whitespace = true
    indent_style = space

    [*.{js,py,html}]
    charset = utf-8

    [*.py]
    indent_size = 4

    [*.{html, js}]
    indent_size = 2


## ipython
* ctrl+r, ctrl+c
* %magic
* %paste
* ?, ??
* In[0], Out[1], _, __
* tab completion

## Example: ipython

## debug

pdbpp, ipdb
* import pdb; pdb.set_trace()
* help
* l, l 20, up, down
* c, s


py.test

    py.test --nomigrations --reuse-db --pdb --maxfail 20
    py.test --nomigrations --reuse-db --looponfail -n 4 --maxfail 20


~/.pdbrc.py

    import pdb

    class Config(pdb.DefaultConfig):
        sticky_by_default = True
        disable_pytest_capturing = False


## debug function

    def debug(f, *args, **kwargs):
        """Launch function debug directly from IPython.

        Source: http://blog.adamdklein.com/?p=533

        Usage:
        # >>> debug(test_function, arg1, arg2, named_arg1='hello')

        """
        from pdb import Pdb as OldPdb
        try:
            from IPython.core.debugger import Pdb
            kw = dict(color_scheme='Linux')
        except ImportError:
            Pdb = OldPdb
            kw = {}
        pdb = Pdb(**kw)
        return pdb.runcall(f, *args, **kwargs)


## Debug example

### data types

https://docs.python.org/2/tutorial/introduction.html

* number, string, unicode
* list, dict, tuple, set
* member access with [:]

### UnicodeDecodeError :(

* .decode(encoding): str -> unicode
* .encode(encoding): unicode -> str
* len(u'zażółćgęśląjaźń'), len('zażółćgęśląjaźń')
* u('zażółćgęśląjaźń') != 'zażółćgęśląjaźń'
* u'zażółćgęśląjaźń' == 'zażółćgęśląjaźń'.decode('utf-8')
* u'zażółćgęśląjaźń'.encode('utf-8') == 'zażółćgęśląjaźń'

There i fixed it
* from __future__ import unicode_literals
* b""
* u""

Problems
default Python 2 encoding is 'ascii'
files might contain a BOM (https://en.wikipedia.org/wiki/Byte_order_mark)
not all Python 2 internals support Unicode
You can't reliably guess an encoding

Advice (http://farmdev.com/talks/unicode/)
* Decode early, Unicode everywhere, encode late
* write wrappers for modules that don't like Unicode (unicodecsv)
* Always put Unicode in unit tests (zażółćgęśląjaźń)
* UTF-8 is the best guess for an encoding (haha!)
* use the BOM to guess encodings
* or use chardet.detect()

### control flow

https://docs.python.org/2/tutorial/controlflow.html

control flow
* if, elif, else
* emulating switch with dict
* while, for, range, iterables
* continue, break, break with else
* pass

functions
* *args
* **kwargs
* named arguments
* defaults are evaluated only once
* unpacking: a, b = (1, 2)
* lambdas

### Data structures

https://docs.python.org/2/tutorial/datastructures.html
https://docs.python.org/2/library/stdtypes.html

common api
* members: get, set
* slicing [1:10], negative indexing

datasctructure api
* list
* dict
* tuple
* set

list comprehensions
* list
* dict
* nesting
* del

### Modules
https://docs.python.org/2/tutorial/modules.html

* file module
* directory module (__init__.py)
* path
* *.pyc

### Formatting

https://docs.python.org/2/tutorial/inputoutput.html
https://docs.python.org/2/library/stdtypes.html#string-formatting


### I/O
https://docs.python.org/2/tutorial/inputoutput.html

* file-like object
* open
* with open
* json


### Exceptions

https://docs.python.org/2/tutorial/errors.html

* built in exceptions
* custom exceptions
* finally
* context processor api

http://www.ianbicking.org/blog/2007/09/re-raising-exceptions.html


### Classes

https://docs.python.org/2/tutorial/classes.html

* always inherit from at least object
* multiple inheritance
* __init__


### Advanced concepts

* iterators
* generators
* context manager (https://docs.python.org/2/library/contextlib.html)
* decorator
* metaclasses

### Standard library

https://docs.python.org/2/library/index.html

itertools
collections


### future

https://docs.python.org/2/library/__future__.html

    from __future__ import division
    from __future__ import absolute_import
    from __future__ import print_function
    from __future__ import unicode_literals

### functional python

What is a function

    str
    dict
    some_dict.get
    some_dict.get
    getattr
    lambda x: x * 2
    sorted, sum

    def my_function(x):
        return x.name

map, reversed, sorted, sum, reduce

# Django web framework

## Django version

https://www.djangoproject.com/weblog/2015/jun/25/roadmap/


https://docs.djangoproject.com/en/1.9/releases/1.9/

## Django design

https://docs.djangoproject.com/en/1.9/misc/design-philosophies/

* Loose coupling
* Don’t repeat yourself (DRY)
* Explicit is better than implicit


## Django conventions

https://docs.djangoproject.com/en/1.8/internals/contributing/writing-code/coding-style/

* Only relative imports in  a django app
* Django app naming

## Django architecture

<img src="http://hitesh.in/wp-content/uploads/2009/12/120909_1217_DjangoFlowc1.png" alt="Django diagram" style="height: 500px;"/>

http://hitesh.in/2009/django-flow/

## Django architecture

MVC? [MTV](https://docs.djangoproject.com/en/1.9/faq/general/#django-appears-to-be-a-mvc-framework-but-you-call-the-controller-the-view-and-the-view-the-template-how-come-you-don-t-use-the-standard-names)

Models - contain business logic, provide database access

Views - define what is presented to the user, process requests

Templates - define how it is presented to the user

Urls - describe routing, map urls to views

## Database

* sqlite may be fine at first, but it's better to use the same engine as in production
* good support for postgresql
* direct access to current database's shell `./manage.py dbshell`
* settings.DATABASES
* multiple databases are possible, but there are certain issues: cross-db transactions, relations
* print suggested models for configured database(s) `./manage.py inspectdb`

## Models

Models represent database entities

* model fields [reference](https://docs.djangoproject.com/en/1.8/ref/models/fields/)
* wrap all strings with translations, use _('some string')
* add Meta
    verbose_name
    verbose_name_plural
    ordering
* add `__unicode__` method

Model concepts

* abstract models
* model inheritance
* use model managers

## Model Managers

Managers contain logic that involves a group of models or model management

* filtering, like custom filter methods
* add specialized `create` method like `create_superuser`, `create_user`
* add custom reports `MyModel.objects.filter(type="sent").recipient_summary()

Tips

* use models.Manager.from_queryset

```python
from django import models

class MyModelQuerySet(models.QuerySet):

    def method_available_on_manager(self):
        return 42

class MyModelManager(models.Manager.from_queryset(MyModelQuerySet)):

    pass
```

## asd

## Urls

Urls define the structure of the project

* use namespaces
* dont use string Views, use
* always use reverse

## Urls example

## Views

Views handle http requests

* use models to access the database
* generate response with the help of templates
* call services and apis
* launch asynchroneous tasks

There are two approaches to views

Function based views (FBV)
Class based views (CBV)

## FBV example

## CBV example

## templates

Template library

* Django template library
* Jinja2

General advice
* it's possible to use both now
* use DTL initially

Inheritance

* how to inherit
* 2, 3 level inheritance

Tips

* use a sane base template
* use existing defaults variables (object, object_list)
* don't inheri too much

## templates - templatetags

Tips
* first check if a tag doesn't already exist
* write tags for formats you use often
* django runserver may require a restart to find the new library
* avoid inclusion templatetags as they are a performance hit


## Where to put logic

Changes record -> Model

Changes on set of models -> Manager

Summary of models of a certain type -> Manager

Generate PDF -> separate helper or a task

Outside script -> Management commands

Mass mail -> celery, rq

# Thank you for listening
