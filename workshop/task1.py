#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

in_path = sys.argv[1]
out_path = sys.argv[2]

print(in_path, out_path)


# 1) wczytać plik linia po linii
with open(in_path) as users_file:
    for line in users_file:
        import pdb; pdb.set_trace()
        print(line)
    pass


# 2) utworzyć słownik dla każdego rekordu
# 3) posortować rekordy po imieniu i wyświetlić
# 4) wypisać do pliku posortowane rekordy, w kolejności kolumn: pesel, first_name, last_name, email
