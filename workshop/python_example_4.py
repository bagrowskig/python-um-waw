#!/usr/bin/env python

test_objects = [
    list(),  # []
    dict(),  # {}
    tuple(),  # (, )
    set(),  # set()
]

for obj in test_objects:
    print(type(obj), obj)


a_list = [1, 2, 3, 4, 'asd', 'zxc']

print('3rd element: ', a_list[3])

for v in a_list:
    print(v)

print('\na_dict')
a_dict = {}
a_dict = {'a': 1, 'b': 2}
a_dict['c'] = 2

for v in a_dict:
    print(v)

for v in a_dict.values():
    print(v)

for v in a_dict.keys():
    print(v)

for v in a_dict.items():
    print(v)

print(a_dict['c'])
print(a_dict.get('c'))
print(a_dict.get('d'), a_dict.get('d', None))
print(a_dict.get('d', 'brak'))

print(a_dict.setdefault('c', 10))
print(a_dict.setdefault('d', 10))

a_tuple = ()


a_set = {, }

