#!/usr/bin/env python
# -*- coding: utf-8 -*-

# print('great success')

# for i in xrange(50):
#     print(i)

for value in (True, False, 0, 1, [], [1, 2]):
    print('\n')
    print(value)
    if value:
        print('value is truish')
    else:
        print('value is not truish')

    if value is True:
        print('ok')
    elif value == 1:
        print(1)
    else:
        print('not ok')


some_list = []
if len(some_list):
    pass  # don't do this

if some_list:
    # TODO: tutaj wysyłamy maila
    pass
else:
    # TODO: tutaj pokazujemy komunikat o braku adresatów
    pass

print("###################")

key = 'u'
print({
    'u': 'Username',
    'p': 'Password',
}[key])


def process_username(value):
    return value

from hashlib import sha256


def process_password(value):
    return sha256(value).hexdigest()

key, value = 'p', '8309fjk2390'
print({
    'u': process_username,
    'p': process_password,
}[key](value))


def parse_value(key, value):
    method_dict = {
        'username': process_username,
        'password': process_password,
    }
    method = method_dict[key]
    return method(value)


def parse_value_2(key, value):
    return {
        'username': process_username,
        'password': process_password,
    }[key](value)


def parse_value_3(key, value):
    return {
        'username': process_username,
        'password': process_password,
    }.get(key, lambda x: x)(value)

print(parse_value('password', '8309fjk2390'))
print(parse_value_2('password', '8309fjk2390'))
print(parse_value_3('address', 'Aleje jerozolimskie'))

for i in range(10):
    if i < 2:
        continue
    print(i)
else:
    print('fin')

for i in range(10):
    print(i)
    if i > 4:
        break
else:
    print('no fin')

print("###################")


for obj in ({'a': 1, 'b': 2}, [1, 2, 3, 4], (4, 5, 6), 'error'):
    for item in obj:
        print(item)


print("###################")


def fun1(arg1, arg2):
    print('arg1: {}, arg2: {}'.format(arg1, arg2))


def fun2(arg1, arg2='default value', arg3='default value 2'):
    print('arg1: {}, arg2: {}, arg3: {}'.format(arg1, arg2, arg3))


fun1(1, 2)

fun2(3)
fun2(3, 4)

fun2(3, arg3='not default argument')
fun2(1, 2, 3)
fun2(arg3=3, arg2=2, arg1=1)

# fun2(**{'arg3': 3})


def what_arguments(*args, **kwargs):
    print('args: ', args)
    print('kwargs: ', kwargs)
print("###################")

what_arguments()
what_arguments(1, 2, 3, 4, 5, 6, 7)
what_arguments(asd=2, zxc=3, cx=4, z=5, v=6, x=7)
what_arguments(1, 2, 3, z=5, v=6, x=7)


def what_arguments_2(arg1, arg2, *args, **kwargs):
    print('arg1: ', arg1)
    print('arg2: ', arg2)
    print('args: ', args)
    print('kwargs: ', kwargs)
    print('len(args): ', len(args))
    print('len(kwargs): ', len(kwargs))

print("###################")

what_arguments_2(1, 2, 3, 4, 5, 6, 7)
# what_arguments_2(1, 2, 3, 4, 5, arg1=6, z=7)
# TypeError: what_arguments_2() got multiple values for keyword argument 'arg1'

# what_arguments_2(arg1=6, z=7, 1, 2, 3, 4, 5)
# SyntaxError: non-keyword arg after keyword arg

print("###################")

my_args = {
    'arg1': 'arg1 value',
    'arg2': 'arg2 value',
    'arg3': 'arg3 value'
}
what_arguments_2(**my_args)
what_arguments_2({1: 1, 2: 2}, {1: 1, 2: 2})


# class MyForm(forms.Form):
class BaseForm(object):

    def __init__(self, instance, data, *args, **kwargs):
        self.instance = instance
        self.init()
        super(BaseForm, self).__init__(*args, **kwargs)


class MyForm(BaseForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('reqest', None)
        super(MyForm, self).__init__(*args, **kwargs)

# request = ''

# form = MyForm(request=request)


def library_method():
    # blackbox
    pass


def wrap_csv(arg1, arg2, *args, **kwargs):
    if arg1 > arg2:
        return
    else:
        return library_method(*args, **kwargs)


def print_args(*args):
    print(args)

print("###################")

print_args(1, 2, 3)
print_args(*range(10))


def print_args(arg1, arg2, arg3, ):
    print(arg1, arg2, arg3)

print_args(1, 2, 3)
print_args(*[1, 2, 3])


print("###################")


def fun_with_defaults(key, value=42, aggregator={}):
    aggregator[key] = value
    return aggregator


print(fun_with_defaults('what is the answer to universe and everything'))
print(fun_with_defaults('what is your name', 'Jan'))

some_dict = {}
print(fun_with_defaults('what is your name', 'Grzegorz', some_dict))
print(fun_with_defaults('what is your name 2', 'Stefan', some_dict))


def fun_with_defaults_proper(key, value=42, aggregator=None):
    if aggregator is None:
        aggregator = {}
    aggregator[key] = value
    return aggregator

print("###################")

print(fun_with_defaults_proper('what is the answer to universe and everything'))
print(fun_with_defaults_proper('what is your name', 'Jan'))

print("###################")

var1, var2, var3 = range(3)
print(var1, var2, var3)

print("###################")


def multiply_by_2(value):
    return value * 2

some_function = lambda x: x * 2

print(some_function(2))
print(multiply_by_2(2))

items = ({'name': 'Michał', 'age': 122}, {'name': 'Jan', 'age': 13}, {'name': 'Andrzej', 'age': 55})
print(items)


def get_age_key(item):
    return item['age']


print(sorted(items, key=get_age_key))
print(sorted(items, key=lambda x: (x['age'])))
