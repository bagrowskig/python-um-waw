long_function_name = 1
var_one, var_two, var_three, var_four = (1, 2, 3, 4)

# Aligned with opening delimiter.
foo = long_function_name(var_one, var_two,
                         var_three, var_four)


# More indentation included to distinguish this from the rest.

def long_function_name(
        var_one, var_two, var_three,
        var_four):
    print(var_one)

# Hanging indents should add a level.
foo = long_function_name(
    var_one, var_two,
    var_three, var_four)

# Arguments on first line forbidden when not using vertical alignment.
foo = long_function_name(var_one, var_two,
                         var_three, var_four)


# Further indentation required as indentation is not distinguishable.
def long_function_name(
        var_one, var_two, var_three,
        var_four):
    """
    """
    print(var_one)


class Asd():
    asd = 1
    asd = 1
    asd = 1
    asd = 1

    class Xxcc():
        pass

    def asd():
        pass

    def asd2():
        pass


def asd():
    pass


def asd2():
    pass

from foo.bar.yourclass import YourClass
from foo.baz.yourclass import YourClass as BazYourClass


asd1 = YourClass()
zxc = BazYourClass()


# import datetime
# from datetime import datetime

# datetime.datetime
# datetime.timedelta

from os import path
# from os import *
# paff

path


# from os import *

path = "/tmp/asd"


# asd = YourClass()
# zxc = bar.yourclass.YourClass()

asd3 = 1  # 1


def add54(number, offset=10):
    """Add 54 to number.

    Keyword arguments:
    * offset -- added to number

    Returns a number

    """
    pass


def f(x): return 2 * x


a = lambda x: 2 * x

var1 = 2


def fun():
    var1 = 2
    var1 += 1
    print(var1)
