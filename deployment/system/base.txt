g++
make
pkg-config
curl

git
mercurial

python
python-dev

libssl-dev

# Pillow
libjpeg-dev

# LDAP
libsasl2-dev
libldap2-dev
